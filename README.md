# What is this?

This is a starting point for using modules from the extensive three.js library. 

If you can run the threejs examples locally, it's easier to remix them into your own projects.

# Install

From the root

``` markdown
# install local server
npm install

# clone three.js repo
cd pub
git clone --depth=1 https://github.com/mrdoob/three.js.git

# install three.js modules
cd three.js
npm install

# back to root
cd ../../

# prepare for setting up ssl cert
mkdir cert
```

# Set up trusted SSL cert for this machine (if not already set up)

- For Windows 10 users

Create the cert that will allow you to use https on localhost projects, including this one (required for some threejs VR modules).

Resource: https://deliciousbrains.com/ssl-certificate-authority-for-local-https-development/

This readme assumes you don't already have openssl on your class path and that you don't feel like adding it, so instead of using "openssl," the full path to your git directory is used: `"C:\Program Files\Git\usr\bin\openssl.exe"`

From the root

``` markdown
# create passphrase file containing the password you will set for your cert 
cd cert
echo "[YOUR-CERT-PASSWORD]" >> phrase.txt

# generate https cert for local server ssl (command prompt)
"C:\Program Files\Git\usr\bin\openssl.exe" genrsa -des3 -out myCA.key 2048
"C:\Program Files\Git\usr\bin\openssl.exe" req -x509 -new -nodes -key myCA.key -sha256 -days 7300 -out myCA.pem
```

You don't want the generated **myCA.pem**, **myCA.key** and **phrase.txt** to hang out under this directory in your local project. So go ahead and move them to a safe place. 

These cert files will be used by *all* of the localhost projects to enable https. 

After moving these files...

In powershell, run as admin:
```
Start-Process powershell -Verb runAs
```

Add your .pem as a trusted "Root":
```
cd [location where you moved myCA.pem and myCA.key]
certutil -addstore "Root" .\myCA.pem
```

<i>Root "Trusted Root Certification Authorities"
Signature matches Public Key
Certificate "[YOUR-NAME]" added to store.
CertUtil: -addstore command completed successfully.</i>

# Generate ssl for this localhost project

This requires that you already have "Set up trusted SSL cert for this machine" (previous section).

From the root

``` markdown
cd cert

# generate .key for this project (command prompt)
"C:\Program Files\Git\usr\bin\openssl.exe" genrsa -out dev.three_js_library_wrapper.key 2048

# generate .csr for this project (command prompt)
"C:\Program Files\Git\usr\bin\openssl.exe" req -new -key dev.three_js_library_wrapper.key -out dev.three_js_library_wrapper.csr
```

You will be prompted for `A challenge password []:` which you can store in `cert/phrase.txt` if you want. But this password isn't used within the scope of this setup nor is needed to run the local server.

Create file, `cert/dev.three_js_library_wrapper.ext` with contents:
```
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names

[alt_names]
DNS.1 = dev.three_js_library_wrapper
```

From the root 

``` markdown
cd cert

# create temporary variable to hold path to myCA...
set myCAPath="[location where you moved myCA.pem and myCA.key]"

# generate .crt for this project (command prompt)
"C:\Program Files\Git\usr\bin\openssl.exe" x509 -req -in dev.three_js_library_wrapper.csr -CA %myCAPath%\myCA.pem -CAkey %myCAPath%\myCA.key -CAcreateserial -out dev.three_js_library_wrapper.crt -days 7300 -sha256 -extfile dev.three_js_library_wrapper.ext
```

You will be prompted to enter the password you originally set for myCA.pem. 

# Add dev.three_js_library_wrapper to your hosts file

- Run notepad as administrator
- Open this file with notepad `C:\Windows\System32\drivers\etc\hosts`
- Add the host for your local IP address, to the end of the file:

```
127.0.0.1       dev.three_js_library_wrapper
```

# Run

From the root

`node server.js`

Server listening at: 

https://dev.three_js_library_wrapper:4000/

# Update Wrapper

From the root

```
git pull
npm install
```

# Update Three.js

From the root

```
cd pub/three.js

git pull
npm install
```
