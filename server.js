const port = 4000;
const host = 'dev.three_js_library_wrapper';
const publicFolder = 'pub';

//server imports
const fs = require('fs');
const express = require('express');
const https = require('https');
const app = express();
const bodyParser = require('body-parser');

const fsOptions = {encoding: 'utf8'};

const keyFile = `${host}.key`;
const certFile = `${host}.crt`;

const key = fs.readFileSync(__dirname + `/cert/${keyFile}`, fsOptions).trim();
const cert = fs.readFileSync(__dirname + `/cert/${certFile}`, fsOptions).trim();

const certOptions = {
  key: key,
  cert: cert
};

//root
app.use(express.static(__dirname+'/'+publicFolder+'/'));
app.use( bodyParser.json() ); // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({ // to support URL-encoded bodies
  extended: true
}));

//requests
// const controller = require('./server/controller'); controller(app);

const server = https.createServer(certOptions, app);

//start up tab
server.listen(port, () => {
    console.log(`Server listening at: `);
    console.log(`https://${host}:${port}/`);
});

module.exports = {app, server};